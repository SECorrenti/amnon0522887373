import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'sec-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  @Input() text: string;
  @Input() type = 'button';
  @Input() disabled: boolean;
  @Output() whenClicked = new EventEmitter<string>();

  click() {
    this.whenClicked.emit(this.text);
  }

}
