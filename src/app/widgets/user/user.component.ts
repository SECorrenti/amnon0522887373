import { Component, Input } from '@angular/core';

@Component({
  selector: 'sec-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {


  @Input() avatar: string;
  @Input() selected: boolean;


}
