import { Component, Input } from '@angular/core';

@Component({
  selector: 'sec-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.scss']
})
export class SelectionComponent {


  @Input() avatar: string;
  @Input() disabled: boolean;

}
