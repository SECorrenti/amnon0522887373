import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { WinnerModel } from 'src/app/models/winner.model';

@Injectable()
export class TicTacToeService {

  private readonly options = [
    [0, 1, 2], [0, 4, 8], [0, 3, 6],
    [1, 4, 7], [2, 4, 6], [2, 5, 8],
    [3, 4, 5], [6, 7, 8]
  ];

  private cells: Array<string> = [
    null, null, null, null, null, null, null, null, null,
  ];

  checkerListener = new Subject<WinnerModel>();

  addSelection(location: number, turn: string): boolean {
    if (this.cells[location]) {
      return false;
    }
    this.cells[location] = turn;
    const winner = this.haveWinner();
    const full = !this.haveEmptyPlaces();
    if (winner) {
      this.checkerListener.next(new WinnerModel(winner, this.cells[winner[0]]));
    } else if (full) {
      this.checkerListener.next(new WinnerModel([-1, -1, -1], ''));
    }
    return true;
  }

  getCells() {
    return this.cells;
  }

  restart() {
    for (let i = 0; i < 9; i++) {
      this.cells[i] = null;
    }
  };

  private haveWinner(): Array<number> {
    return this.options.find(line => {
      const val1 = this.cells[line[0]];
      const val2 = this.cells[line[1]];
      const val3 = this.cells[line[2]];
      return !!val1 && !!val2 && !!val3 && val1 === val2 && val2 === val3;
    });
  }

  private haveEmptyPlaces() {
    return this.cells.some(data => !data);
  }


}
