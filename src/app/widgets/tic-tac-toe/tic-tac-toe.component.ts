import { Component, OnInit, Input, Output, EventEmitter, AfterContentInit, OnChanges } from '@angular/core';
import { TicTacToeService } from './tic-tac-toe.service';
import { DynamicPageAnimation } from 'src/app/dialogs/dynamic-dialog/dynamic-dialog.animation';
import { WinnerModel } from 'src/app/models/winner.model';

@Component({
  selector: 'sec-tic-tac-toe',
  templateUrl: './tic-tac-toe.component.html',
  styleUrls: ['./tic-tac-toe.component.scss']
})
export class TicTacToeComponent implements AfterContentInit, OnChanges {



  startAnimation: boolean;
  cells: Array<string> = [];
  @Input() winner: WinnerModel;
  @Input() turn: string;
  @Output() switchPlayer = new EventEmitter<string>();

  constructor(private ticTacToeService: TicTacToeService) {
    this.cells = this.ticTacToeService.getCells();
  }

  ngOnChanges(changes: import("@angular/core").SimpleChanges): void {
    console.log(changes);
  }

  ngAfterContentInit(): void {
    setTimeout(() => this.startAnimation = true);
  }

  click(location: number) {
    if (this.winner) {
      return;
    }
    const success = this.ticTacToeService.addSelection(location, this.turn);
    if (success) {
      this.switchPlayer.emit(this.turn);
    }
  }

}
