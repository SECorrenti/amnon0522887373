import { NgModule } from '@angular/core';
import { UserComponent } from './user/user.component';
import { ButtonComponent } from './button/button.component';
import { CommonModule } from '@angular/common';
import { TicTacToeService } from './tic-tac-toe/tic-tac-toe.service';
import { SelectionComponent } from './selection/selection.component';
import { TicTacToeComponent } from './tic-tac-toe/tic-tac-toe.component';

const declarations = [
  UserComponent,
  ButtonComponent,
  TicTacToeComponent,
  SelectionComponent,
];

const imports = [
  CommonModule,
];

const providers = [
  TicTacToeService
];

@NgModule({
    declarations,
    imports,
    providers,
    exports: declarations,
})
export class WidgetsModule { }
