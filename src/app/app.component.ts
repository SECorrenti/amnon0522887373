import { Component } from '@angular/core';
import { DynamicPageAnimation } from './dialogs/dynamic-dialog/dynamic-dialog.animation';

@Component({
  selector: 'sec-root',
  template: [
    '<div [@routerTransition]=\'o.isActivated ? o.activatedRouteData["state"] : null\'>',
      '<router-outlet #o=\'outlet\'></router-outlet> ',
    '</div>'].join(''),
  animations: [DynamicPageAnimation.scaleTransition()],
})
export class AppComponent {

}
