import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { IDynamicDialog } from '../dynamic-dialog/dynamic-dialog.interface';

@Component({
  selector: 'sec-choose-avatar',
  templateUrl: './choose-avatar.component.html',
  styleUrls: ['./choose-avatar.component.scss']
})
export class ChooseAvatarDialogComponent implements IDynamicDialog {

  whenDone = new Subject<string>();

  close(img: string): void {
    this.whenDone.next(img);
  }

}
