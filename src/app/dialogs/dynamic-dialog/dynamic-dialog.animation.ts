import {
  trigger, style, state, transition,
  animate, keyframes, query, group
} from '@angular/animations';

export class DynamicPageAnimation {

  static scaleTransition() {
    return trigger('routerTransition', [
      transition('* <=> *', [
        query(':enter, :leave', style({
          transform: 'translateY(101%)',
        }), { optional: true }),
        group([
          query(':enter', [
            animate('.8s ease', keyframes([
              style({
                transform: 'translateY(101%)',
                opacity: 0,
              }),
              style({
                transform: 'translateY(0%)',
                opacity: 1,
              }),
            ])),
          ], { optional: true }),
          query(':leave', [
            animate('.4s ease-in-out', keyframes([
              style({
                transform: 'translateY(0%)',
                opacity: 1,
              }),
              style({
                transform: 'translateY(-101%)',
                opacity: 0,
              }),
            ]))
          ], { optional: true }),
        ])
      ])
    ])
  }


  public static fadeInAnim(): DynamicPageAnimation {
    return trigger('fadeInAnim', [
      state('in', style({
        opacity: 1,
      })),
      transition('void => *', [
        style({
          opacity: 0,
          'background-color': 'black',
        }),
        animate(300)
      ]),
    ]);
  }
}
