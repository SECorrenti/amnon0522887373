
import { NgModule } from '@angular/core';
import { DynamicDialogComponent } from './dynamic-dialog.component';
import { DynamicDialogService } from './dynamic-dialog.service';

const entryComponents = [
  DynamicDialogComponent,
];

const declarations = [
  ...entryComponents
];

const providers = [
  DynamicDialogService,
];

@NgModule({
  declarations,
  providers,
  entryComponents,
  exports: declarations,
})
export class DynamicDialogModule { }
