import {
  Component, ViewChild,
  ViewContainerRef, ComponentFactoryResolver,
  Renderer2, Type, ComponentRef, ElementRef, AfterContentInit,
} from '@angular/core';
import { DynamicPageAnimation } from './dynamic-dialog.animation';
import { Subscription } from 'rxjs';

@Component({
  selector: 'sec-dynamic-dialog',
  templateUrl: './dynamic-dialog.component.html',
  styleUrls: ['./dynamic-dialog.component.scss'],
  animations: [
    DynamicPageAnimation.fadeInAnim(),
  ]
})
export class DynamicDialogComponent implements AfterContentInit {

  constructor(
    private resolver: ComponentFactoryResolver,
    private renderer: Renderer2,
  ) {
  }

  private dialogRef: any;
  scale = 'bigger';
  @ViewChild('animPage') animPage: ElementRef;
  @ViewChild('animPopup') animPopup: ElementRef;
  @ViewChild('viewContainer', { read: ViewContainerRef }) container: ViewContainerRef;

  addTemplate<C>(componentType: Type<C>, dialog: any): ComponentRef<C> {
    this.dialogRef = dialog;
    const factory = this.resolver.resolveComponentFactory(componentType) as any;
    return this.container.createComponent(factory);
  }

  ngAfterContentInit(): void {
    setTimeout(() => {
      this.renderer.removeClass(this.animPopup.nativeElement, 'scale');
    }, 0);
  }

  destroy(sub: Subscription) {
    sub.unsubscribe();
    this.renderer.addClass(this.animPopup.nativeElement, 'hide');
    this.renderer.addClass(this.animPage.nativeElement, 'hide');
    this.dialogRef.destroy();
  }



}
