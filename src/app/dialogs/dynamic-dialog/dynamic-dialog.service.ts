import { Injectable, Type, ComponentFactoryResolver, Inject, ApplicationRef, Injector, EmbeddedViewRef, ComponentRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { DynamicDialogComponent } from './dynamic-dialog.component';
import { IDynamicDialog } from './dynamic-dialog.interface';

@Injectable({ providedIn: 'root' })
export class DynamicDialogService {

  readonly body: HTMLBodyElement;
  private dialog: Array<ComponentRef<DynamicDialogComponent>> = [];

  constructor(private resolver: ComponentFactoryResolver,
    private appRef: ApplicationRef,
    private injector: Injector,
    @Inject(DOCUMENT) private document: Document) {
    this.body = this.document.querySelector('body');
  }

  open(componentType: Type<IDynamicDialog>): {
    dialog: DynamicDialogComponent,
    component: IDynamicDialog
  } {
    setTimeout(() => this.document.body.style.overflow = 'hidden', 500);
    const dialogFactory = this.resolver.resolveComponentFactory(DynamicDialogComponent);
    const dialog = dialogFactory.create(this.injector);
    this.dialog.push(dialog);
    dialog.destroy = () => this.dettach(dialog);
    const component = dialog.instance.addTemplate(componentType, dialog);
    this.appRef.attachView(dialog.hostView);
    this.body.appendChild((dialog.hostView as EmbeddedViewRef<any>).rootNodes[0]);
    return { dialog: dialog.instance, component: component.instance };
  }

  dettach(dialog) {
    setTimeout(() => {
      this.document.body.style.overflow = '';
      this.body.removeChild((dialog.hostView as EmbeddedViewRef<any>).rootNodes[0]);
      this.appRef.detachView(dialog.hostView);
    }, 700);
  }

}



