import { Subject } from 'rxjs';


export interface IDynamicDialog {
    whenDone: Subject<string>;
}

