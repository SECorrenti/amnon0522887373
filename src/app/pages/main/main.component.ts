import { Component } from '@angular/core';
import { FormControl, Validators, FormArray } from '@angular/forms';
import { DynamicDialogService } from '../../dialogs/dynamic-dialog/dynamic-dialog.service';
import { DynamicDialogComponent } from '../../dialogs/dynamic-dialog/dynamic-dialog.component';
import { ChooseAvatarDialogComponent } from 'src/app/dialogs/choose-avatar/choose-avatar.component';
import { Router } from '@angular/router';

@Component({
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {

  constructor(
    private dynamicDialogService: DynamicDialogService,
    private router: Router,
  ) { }

  form = new FormArray([
    new FormControl('', Validators.required),
    new FormControl('', Validators.required),
  ]);

  selectUser(location: number) {
    const { dialog, component } = this.dynamicDialogService.open(ChooseAvatarDialogComponent);
    const sub = component.whenDone.subscribe(avatar => {
      this.form.controls[location].setValue(avatar);
      dialog.destroy(sub);
    });
  }

  whenSubmit() {
    this.router.navigate(['game',
      this.form.controls[0].value,
      this.form.controls[1].value
    ]);
  }
}
