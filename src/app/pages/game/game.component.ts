import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TicTacToeService } from 'src/app/widgets/tic-tac-toe/tic-tac-toe.service';
import { Subscription } from 'rxjs';
import { WinnerModel } from 'src/app/models/winner.model';

@Component({
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnDestroy {

  private readonly subs = new Subscription();

  player1: string;
  player2: string;
  turn: string;
  winner: WinnerModel;

  constructor(
    route: ActivatedRoute,
    private ticTacToeService: TicTacToeService) {
    this.player1 = route.snapshot.params.player1;
    this.player2 = route.snapshot.params.player2;
    this.renew();
    this.subs.add(ticTacToeService.checkerListener
      .subscribe(winner => this.winner = winner));
  }

  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }

  switchPlayer(player: string) {
    this.turn = player === this.player1 ? this.player2 : this.player1;
  }

  renew() {
    this.ticTacToeService.restart();
    this.turn = this.player1;
    this.winner = null;
  }
}

