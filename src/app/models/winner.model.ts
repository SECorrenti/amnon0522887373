
import { UserModel } from './user.model';


export class WinnerModel extends UserModel {

    constructor(
      public cells: Array<number>,
      avatar: string
    ) {
      super(avatar);
    }

}
