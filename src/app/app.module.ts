import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { AppRoutes } from './app.routing';
import { MainComponent } from './pages/main/main.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DynamicDialogComponent } from './dialogs/dynamic-dialog/dynamic-dialog.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChooseAvatarDialogComponent } from './dialogs/choose-avatar/choose-avatar.component';
import { GameComponent } from './pages/game/game.component';
import { WidgetsModule } from './widgets/widgets.module';

const entryComponents = [
    ChooseAvatarDialogComponent,
    DynamicDialogComponent,
];

const declarations = [
    AppComponent,
    MainComponent,
    GameComponent,
    ...entryComponents,
];

const imports = [
    WidgetsModule,
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes)
];

@NgModule({
    declarations,
    entryComponents,
    imports,
    providers: [],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
