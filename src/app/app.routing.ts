import { Routes } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { GameComponent } from './pages/game/game.component';


export const AppRoutes: Routes = [
  { path: '', component: MainComponent, pathMatch: 'full', data: { state: 'main-state'}},
  { path: 'game/:player1/:player2', component: GameComponent, data: { state: 'game-state'} },
  { path: '**', redirectTo: '' }
];
